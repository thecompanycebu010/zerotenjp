<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- recruit page -->
<section class="zt-recruit-page">
    <div class="cntr-750">
        <div class="zt-home-title">
            <h2>Recruit</h2>
        </div>
       <div class="zt-recruit-title">
           <h3>国内事業</h3>
       </div>
       <div class="zt-recruit-dl is-a">
            <dl>
                <dt><p>募集職種</p></dt>
                <dd>
                    <p class="mb-30 mb-0-xs">1)クリエイター</p>
                    </p>2)コワーキングスペーススタッフ</p>
                </dd>
            </dl>
            <dl>
                <dt><p>業務内容</p></dt>
                <dd>
                    <p>(1)クリエイター<br>
                        <span>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</span><br>
                        <br>
                        2)コワーキングスペーススタッフ<br>
                        <span>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</span>
                    </p>
                </dd>
            </dl>
            <dl>
                <dt><p>応募資格</p></dt>
                <dd>
                    <p>実務経験者（もしくはそれと同等のスキルをお持ちの方）</p>
                </dd>
            </dl>
            <dl>
                <dt><p>募集人数</p></dt>
                <dd>
                    <p>若干名</p>
                </dd>
            </dl>
            <dl>
                <dt><p>雇用形態</p></dt>
                <dd>
                    <p>社員(試用期間あり)、契約社員</p>
                </dd>
            </dl>
            <dl>
                <dt><p>勤務地</p></dt>
                <dd>
                    <p>(1)福岡県福岡市博多区祇園町8-13 第一プリンスビル The Company1F<br>
                        (2)福岡県内店舗をローテーション、または熊本店
                    </p>
                </dd>
            </dl>
            <dl>
                <dt><p>勤務時間</p></dt>
                <dd>
                    <p>(1)フレックスタイム制<br>(2)9:00~21:00(シフト制)</p>
                </dd>
            </dl>
            <dl>
                <dt><p>給与</p></dt>
                <dd>
                    <p>経験、能力などを考慮し、協議の上決定</p>
                </dd>
            </dl>
            <dl>
                <dt><p>待遇</p></dt>
                <dd>
                    <p>社会保険・厚生年金・雇用保険・昇給制度・決算賞与・定期健康診断あり</p>
                </dd>
            </dl>
            <dl>
                <dt><p>休日・休暇</p></dt>
                <dd>
                    <p>週休２日制(土・日曜日)、祝日、年末年始、その他有給休暇、特別休暇</p>
                </dd>
            </dl>
        </div>
    </div>
    <div class="zt-recruit-p">
        <div class="zt-rec-z">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/zero1.png" alt="">
        </div>
        <div class="cntr-750">
            <div class="zt-recruit-title">
                <h3>国外事業</h3>
            </div>
            <div class="zt-recruit-dl">
                <dl>
                    <dt><p>募集職種</p></dt>
                    <dd>
                        <p class="mb-30 mb-0-xs">(1)コワーキングスペーススタッフ</p>
                        <p>(2)店舗開拓</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>業務内容</p></dt>
                    <dd>
                        <p>(1)コワーキングスペーススタッフ<br>
                            <span>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</span><br>
                            <br>
                            (2)店舗開拓<br>
                            <span>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</span>
                        </p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>応募資格</p></dt>
                    <dd>
                        <p>実務経験者（もしくはそれと同等のスキルをお持ちの方）</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>募集人数</p></dt>
                    <dd>
                        <p>若干名</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>雇用形態</p></dt>
                    <dd>
                        <p>社員(試用期間あり)、契約社員</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>勤務地</p></dt>
                    <dd>
                        <p>フィリピン、シンガポール</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>勤務時間</p></dt>
                    <dd>
                        <p>(1)9:00~21:00(シフト制)<br>(2)フレックスタイム制</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>給与</p></dt>
                    <dd>
                        <p>経験、能力などを考慮し、協議の上決定</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>待遇</p></dt>
                    <dd>
                        <p>社会保険・厚生年金・雇用保険・昇給制度・決算賞与・定期健康診断あり</p>
                    </dd>
                </dl>
                <dl>
                    <dt><p>休日・休暇</p></dt>
                    <dd>
                        <p>週休２日制(土・日曜日)、祝日、年末年始、その他有給休暇、特別休暇</p>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="zt-recruit-applctn">
        <div class="cntr-750">
            <div class="zt-recruit-title">
                <h3>応募方法</h3>
            </div>
            <div class="zt-applctn-cntnt">
                <p>下記メールフォームから、履歴書・職務経歴書・自己PRをお送りください。<br>
                    書類選考の上、追ってご連絡いたします。<br>
                    ※1週間経過しても連絡がない場合は、メールが正常に送信されなかった可能性がありますので、お手数ですが、 <br class="zt-mn-sp-br">recruit@zeroten.jp または  <br class="zt-mn-sp-br">092-292-4944 までご連絡ください。
                </p>
            </div>
            <div class="zt-recruit-form">
                <!-- <form action="#">
                    <div class="zt-rec-frm">
                        <label for="">お名前<span class="zt-req-rec">*</span></label>
                        <input type="text" name="" id="" placeholder="零点太郎">
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">メールアドレス<span class="zt-req-rec">*</span></label>
                        <input type="text" name="" id="" placeholder="t_zeroten@aaa.com">
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">履歴書<span class="zt-req-rec">*</span></label>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/btn_file.png" alt="">
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">自己PR<span class="zt-req-rec">*</span></label>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/btn_file.png" alt="">
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">職務経歴書<span class="zt-req-rec">*</span></label>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/btn_file.png" alt="">
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">自己PR<span class="zt-req-rec">*</span></label>
                        <textarea name="" id="" cols="30" rows="10" placeholder="自己PRをご記入ください。"></textarea>
                    </div>
                    <div class="zt-rec-frm">
                        <label for="">備考</label>
                        <textarea name="" id="" cols="30" rows="10" placeholder="備考やご質問などありましたらご記入ください。"></textarea>
                    </div>
                    <button class="zt-rec-submit">Submit</button>
                </form> -->
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                        the_content();
                endwhile; else: ?>
                <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- end of recruit page -->

<script>
    $("#files").change(function() {
  filename = this.files[0].name
  console.log(filename);
});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div>
      <label for="files" class="btn">Select Image</label>
      <input id="files" style="visibility:hidden;" type="file">
    </div>
    
<label class="btn btn-primary">
<i class="fa fa-image"></i> Your text here<input type="file" style="display: none;" name="image">
</label>
<?php
get_footer();