/*------------------------------
Mobile Menu (toggleClass to activate transitions)
------------------------------*/
(function($){

    var divs = $(".grid .grid-item");
    for(var i = 0; i < divs.length; i+=6) {
      divs.slice(i, i+6).wrapAll("<li class='grid-list'></li>");
    }

    const navbar = $('.navbar-toggler')

    navbar.on("click",function(e){
        e.preventDefault()
        if( $(".navbar-type").hasClass("is-toggled")) {
            $(this).removeClass("is-toggled")
            $(".navbar-type").removeClass("is-toggled")
            $(".menu-header").removeClass("is-toggled")
        }else{
            $(this).addClass("is-toggled")
            $(".navbar-type").addClass("is-toggled")
            $(".menu-header").addClass("is-toggled")
        }
       
        return false
    })

    let navbarH = $('.navbar').innerHeight();
    let w = $(window).width();

    if( w < 769 ){
        $('.navbar-collapse').css({
            'margin-top': navbarH
        })
    }

    $(window).resize(function(){
        if( w < 769 ){
            $('.navbar-collapse').css({
                'margin-top': navbarH
            })
        }
    })

    $('.zt-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
            $(".menu-header").addClass("sticky");
        } else {
            $(".menu-header").removeClass("sticky");
        }
    });

    // News Dropdown
    let sel_news = $("#zt_selected_news"),
    news_cat_sel = $("#zt_news_cat_sel"),
    news_drop_item = $(".zt_news_drop_item"),
    news_val = ""

    sel_news.on("click",function(e){
        news_cat_sel.slideToggle()
    })
    news_drop_item.on('click',function(e){
        e.preventDefault()
        news_val = $(this).attr("data-val")
        
        window.location.href = news_val
        
        return false
    })


    if($(window).innerWidth()<768) {
        $(document).on('click','.cat-close',function(e){
            e.preventDefault();
            if($(this).find('.sp-toggle-cat').hasClass('is-open')) {
                $(this).find('.sp-toggle-cat').removeClass('is-open');
            }else{
                $(this).find('.sp-toggle-cat').addClass('is-open');
            }
            $(this).next().slideToggle();
            return false;
        });
    }
    
    

})(jQuery);
