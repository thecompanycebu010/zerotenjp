<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- works-detail page -->
<section class="zt-works-dtl-page">
       <div class="zt-sec1">
            <div class="cntr">
                <div class="zt-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_01.png" alt="" class="is-wide">
                </div>
            </div>
            <div class="zt-grd cntr">
                <div class="zt-grd-4 zt-dtl-wrkdtl">
                    <div class="zt-dt-wrkdtl">
                        <p>Sep, 2018</p>
                    </div>
                    <div class="zt-cat-wrkdtl">
                        <p>category<span>Event</span></p>
                    </div>
                    <div class="zt-tag-wrkdtl">
                        <p>tag<span>Lighting / Design / Visual</span></p>
                    </div>
                </div>
                <div class="zt-grd-8">
                    <div class="zt-lft-bg">
                        <div class="zt-ttl">
                            <h4>光と音が織りなす幻想祭 <br class="zt-mn-sp-br">「平戸、海のものがたり」</h4>
                        </div>
                        <div class="zt-mn-cntnts cntr">
                            <p>平戸城本丸のプロジェクションマッピングを始め、シルク・ドゥ・ソレイユ出演パフォーマーを起用した特別演出ショーなど、映像と光と音、パフォーマンスが融合する、平戸ならではの「体感型・デジタルアート」の夜間イベントの総合制作・プロデュースを行いました。</p>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    <div class="cntr">   
        <div class="zt-sec2">
            <div class="zt-cntr">
                <div class="zt-grd">
                    <div class="zt-grd-6 gap35">
                        <div class="zt-wrdtl-bg">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_02.png" alt=""    class="is-wide">
                        </div>
                    </div>
                    <div class="zt-grd-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_03.png" alt="" class="is-wide">
                    </div>
                </div>
            </div>
       </div>
    </div>   
    <div class="zt-wrkdtl-z-2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/icons/ico_02.png" alt="">
    </div>
    <div class="zt-sec3">
        <div class="cntr">
            <div class="zt-img1">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_04.png" alt="" class="is-wide">
            </div>
            <div class="zt-wrkdtl-z-3">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/zero.png" alt="">
            </div>
            <div class="zt-img2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_05.png" alt="" class="is-wide">
            </div>
        </div>
    </div>
    <div class="zt-sec4 cntr">
        <div class="zt-img1">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_06.png" alt="">
        </div>
        <div class="zt-grd zt-img2">
            <div class="zt-grd-6 gap35">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_07.png" alt="" class="is-wide">
                <div class="zt-bg"></div>
            </div>
            <div class="zt-grd-4">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_08.png" alt="" class="is-wide">
            </div>
        </div>
    </div>
    <div class="zt-sec5 cntr">
        <div class="zt-img1">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_09.png" alt="">
        </div>
        <div class="zt-wrkdtl-z-5">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/icons/ico_01.png" alt="">
        </div>
        <div class="zt-grd zt-img2">
            
            <div class="zt-grd-6 gap35">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_10.png" alt="" class="is-wide">
            </div>
            <div class="zt-grd-4">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/detail/img_11.png" alt="" class="is-wide">
            </div>
        </div>
    </div>
   <button class="zt-wrk-dtl-btn">back</button>
</section>
<!-- end of works-detail page -->

<?php
get_footer();