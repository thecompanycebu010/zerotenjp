<?php
/**
 * The template for displaying home pages
 * Template Name: Homepage
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- pager -->
<div class="zt-pager">
    <span class="zt-pager-line"></span>
</div>
<!-- pager -->

<!-- news -->
<section class="zt-news">
    <span class="zt-bubble"></span>
    <div class="cntr-1000">
        <div class="zt-home-title">
            <h2>News</h2>
        </div>
        <?php
        // news query
        $news_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>3)); ?>
        <?php if ( $news_query->have_posts() ) : ?>
            <div class="gap gap-10 gap-0-xs">
                <!-- the loop -->
                <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                    <div class="md-12 xs-12">
                        <a href="<?php the_permalink(); ?>" class="zt-news-card">
                            <div class="zt-news-cont">
                                <div class="zt-news-date-cat">
                                    <span class="zt-news-date"><?php the_time('Y m.d'); ?></span>
                                        <?php
                                            $terms = get_the_terms( $post->ID , 'category' );
                                            if(is_array($terms) || is_object($terms)){
                                                foreach ( $terms as $term ) {
                                                    ?>
                                                    <span class="zt-news-cat <?php echo $term->slug; ?>">
                                                        <?php echo $term->name; ?>
                                                    </span>
                                                <?php
                                                }
                                            }
                                        ?>
                                </div>
                                <h4><?php the_title(); ?></h4>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->
            </div>
            <div class="zt-read-more">
                <a href="<?php bloginfo('url'); ?>/news" class="checkBox">
                    <span>More</span>
                    <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0" class="button" width="" height="65"/>
                    <rect x="0" y="22.5" class="box" width="20" height="20"/>
                    <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                    </svg>
                </a>
            </div>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <h4 class="zt-no-post"><?php _e( 'Sorry, no news matched your criteria.' ); ?></h4>
        <?php endif; ?>
    </div>
</section>
<!-- end of news -->

<!-- pager -->
<div class="zt-pager zt-pager-50">
    <span class="zt-pager-line zt-pager-line-50"></span>
</div>
<!-- pager -->

<!-- servies -->
<section class="zt-services">
    <div class="cntr-1050">
        <div class="zt-home-title">
            <h2>Services</h2>
        </div>
        <div class="gap gap-10 gap-0-xs">
            <div class="md-6 xs-12">
                <a href="#" class="zt-service-card zt-service-card-creative">
                    Creative
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="zt-service-card zt-service-card-space">
                    Co-Working Space
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="zt-service-card zt-service-card-business">
                    Business Matching
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="#" class="zt-service-card zt-service-card-web">
                    Web Solution
                </a>
            </div>
        </div>
    </div>
</section>
<!-- end of services -->

<!-- pager -->
<div class="zt-pager">
    <span class="zt-pager-line zt-pager-line-75"></span>
</div>
<!-- pager -->

<!-- recruit -->
<section class="zt-recruit">
    <div class="cntr">
        <div class="zt-home-title">
            <h2>Recruit</h2>
        </div>
        <p class="zt-tag">
            Zero-Tenグループでは共に働く仲 間を募集しています！<br>
            この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。<br>
            この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。
        </p>
    </div>
    <div class="zt-recruit-img-wrp">
        <div class="zt-det">
            <div class="zt-read-more">
                <a href="<?php bloginfo('url'); ?>" class="checkBox">
                    <span>Detail</span>
                    <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                    <rect x="0" class="button" width="" height="65"/>
                    <rect x="0" y="22.5" class="box" width="20" height="20"/>
                    <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                    </svg>
                </a>
            </div>
        </div>
        <div class="zt-img">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/recruit/img01.jpg" alt="">
        </div>
    </div>
</section>
<!-- end of recruit -->


<?php
get_footer();