

<section class="zt-news-detail">
    <div class="cntr-1000">
        <div class="zt-news-date-cat">
            <span class="zt-det-date"><?php the_date('Y.m.d'); ?></span>
            <span class="zt-det-cat">
                <?php
                    $terms = get_the_terms( $post->ID , 'category' );
                    if(is_array($terms) || is_object($terms)){
                        foreach ( $terms as $term ) {
                        echo $term->name;
                        }
                    }
                ?>
            </span>
        </div>
        <h1 class="news-det-title"><?php the_title(); ?></h1>
        <div class="news-det-cont">
            <?php the_content(); ?>
        </div>
    </div>
</section>

<section class="zt-news-related-post">
    <div class="cntr-1000">
        <div class="zt-home-title">
            <h2>Related Post</h2>
        </div>
        <?php $orig_post = $post;
            global $post;
            $categories = get_the_category($post->ID);
            if ($categories) {
            $category_ids = array();
            foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

            $args=array(
            'category__in' => $category_ids,
            'post__not_in' => array($post->ID),
            'posts_per_page'=> 3, // Number of related posts that will be shown.
            'ignore_sticky_posts'=>1
            );

            $my_query = new wp_query( $args );
            if( $my_query->have_posts() ) {
            echo '<div class="gap gap-10 gap-0-xs">';
                while( $my_query->have_posts() ) {
                $my_query->the_post();?>
                <div class="md-12 xs-12">
                    <a href="<?php the_permalink()?>" class="zt-news-card">
                        <div class="zt-news-cont">
                            <div class="zt-news-date-cat">
                                <span class="zt-news-date"><?php the_date('Y.m.d'); ?></span>
                                <?php
                                    $terms = get_the_terms( $post->ID , 'category' );
                                    if(is_array($terms) || is_object($terms)){
                                        foreach ( $terms as $term ) {
                                            ?>
                                            <span class="zt-news-cat <?php echo $term->slug; ?>">
                                                <?php echo $term->name; ?>
                                            </span>
                                        <?php
                                        }
                                    }
                                ?>
                            </div>
                            <h4><?php the_title(); ?></h4>
                        </div>
                    </a>
                </div>
            <?
            }
            echo '</div>';
            }
            }
            $post = $orig_post;
            wp_reset_query(); 
        ?>
    </div>
</section>