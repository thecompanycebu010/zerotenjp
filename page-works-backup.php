<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- works page -->
<section class="zt-works-page" onclick="myFunction()">
   <div class="zt-wrks-bg">
        <div class="cntr">
            <div class="gap gap-80 gap-0-xs">
                <div class="md-3 xs-12">
                    <h3><span>year</span></h3>
                </div>
                <div class="md-3 xs-12">
                    <h3><span>category</span></h3>
                </div>
                <div class="md-6 xs-12">
                    <h3 class="is-a"><span>tag</span></h3>
                </div>
            </div>
        </div>
   </diV>
   <div id="zt-shw">
        <div class="cntr">
            <div class="gap gap-80 gap-0-xs">
                <div class="md-3 xs-12">
                    <ul class="zt-year">
                        <li>2019</li>
                        <li>2018</li>
                        <li>2017</li>
                        <li>2016</li>
                        <li>2015</li>
                        <li>2014</li>
                        <li>2013</li>
                    </ul>
                </div>
                <div class="md-3 xs-12">
                    <ul class="zt-category">
                        <li>App</li>
                        <li>Exhibition</li>
                        <li>Graphic Design</li>
                        <li>Installation</li>
                        <li>Live</li>
                        <li>Music Video</li>
                        <li>Web</li>
                    </ul>
                </div>
                <div class="md-6 xs-12">
                    <div class="gap gap-20 gap-0-xs">
                        <div class="md-6 xs-12">
                            <ul class="zt-tag">
                                <li>3D Scan</li>
                                <li>AR</li>
                                <li>Data visualation</li>
                                <li>Drone</li>
                                <li>Dynamic Projection Mapping</li>
                                <li>Dynamic VR Display</li>
                                <li>Holographic</li>
                            </ul>
                        </div>
                        <div class="md-6 xs-12">
                            <ul class="zt-tag">
                                <li>Live Streaming</li>
                                <li>Motion Capture</li>
                                <li>Multi Projection</li>
                                <li>Open Source</li>
                                <li>Social</li>
                                <li>Visual</li>
                                <li>AR</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
   <div class="cntr">
       <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
            <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img.png" alt="" class="is-wide">
                    <div class="zt-overlay">
                        <div class="zt-txt">
                            <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                            <h3>「磯崎新の謎」展</h3>
                        </div>
                    </div>
                </div>
        </a>
       <div class="gap gap-10 gap-0-xs mb-20 mb-20-xs">
            <div class="md-6 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_01.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アニメーションサーカス「CLONES」</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                        <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_02.png" alt="" class="is-wide">
                            <div class="zt-overlay">
                                <div class="zt-txt">
                                    <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                    <h3 class="is-a">アニメーションサーカス「CLONES」</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
        </div>
        <div class="gap gap-10 gap-0-xs mb-20 mb-20-xs">
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_03.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_04.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_05.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="grd-itm mb-20 mb-20-xs">
            <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_06.png" alt="" class="is-wide">
                    <div class="zt-overlay">
                        <div class="zt-txt">
                            <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                            <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                        </div>
                    </div>
                </div>
            </a>
       </div>
       <div class="gap gap-10 gap-0-xs mb-20 mb-20-xs">
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_07.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_08.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_09.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="gap gap-10 gap-0-xs mb-20 mb-20-xs">
            <div class="md-6 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_10.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="md-6 xs-12">
                <a href="<?php echo ( esc_url( home_url( '/' ) ) ); ?>works-detail">
                    <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/img_11.png" alt="" class="is-wide">
                        <div class="zt-overlay">
                            <div class="zt-txt">
                                <span class="zt-dt-tm">2019.09.27 - 11.24</span>
                                <h3 class="is-a">アジアフォーカス・福岡国際映画祭</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
   </div>
</section>
<!-- end of works page -->

<script>
    function myFunction() {
    var x = document.getElementById("zt-shw");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
    }
</script>

<?php
get_footer();