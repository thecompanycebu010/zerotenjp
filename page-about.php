<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- about page -->
<section class="zt-about-page">
    <div class="cntr-750">
        <div class="zt-home-title">
            <h2>About</h2>
        </div>
      
        <div class="zt-about">
            <div class="zt-abt-z">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/zero1.png" alt="">
            </div>
            <div class="zt-about-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/zt_logo.png" alt="">
            </div>
            <div class="zt-about-dl">
                <dl>
                    <dt><p>Company Name</p></dt>
                    <dd><p>株式会社Zero-Ten</p></dd>
                </dl>
                <dl>
                    <dt><p>ADDRESS</p></dt>
                    <dd><p>〒812-0038<br>福岡県福岡市博多区祇園町8-13 第一プリンスビル The Company1F</p></dd>
                </dl>
                <dl>
                    <dt><p>ADDRESS</p></dt>
                    <dd><p>〒812-0038<br>福岡県福岡市博多区祇園町8-13 第一プリンスビル The Company1F</p></dd>
                </dl>
                <dl>
                    <dt><p>CAPITAL</p></dt>
                    <dd><p>1,000万円</p></dd>
                </dl>
                <dl>
                    <dt><p>CEO</p></dt>
                    <dd><p>榎本二郎</p></dd>
                </dl>
                <dl>
                    <dt><p>SEVICE</p></dt>
                    <dd>
                        <ul class="zt-list-mn">
                            <li>プロジェクションマッピング、CG、3DCG、動画などの映像制作</li>
                            <li>イルミーション、装飾、照明などの空間演出</li>
                            <li>各種デザイン、グラフィック制作</li>
                            <li>各種イベント企画、運営</li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="zt-zt-park">
        <div class="zt-abt-z is-a">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/zero.png" alt="">
        </div>
        <div class="cntr-750">
            <div class="zt-zt-p-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/ztp_logo.png" alt="">
            </div>
            <div class="zt-about-dl">
                <dl>
                    <dt><p>Company Name</p></dt>
                    <dd><p>株式会社Zero-Ten Park</p></dd>
                </dl>
                <dl>
                    <dt><p>ADDRESS</p></dt>
                    <dd><p>〒812-0038<br>福岡県福岡市博多区祇園町8-13 第一プリンスビル The Company1F</p></dd>
                </dl>
                <dl>
                    <dt><p>CAPITAL</p></dt>
                    <dd><p>1,000万円</p></dd>
                </dl>
                <dl>
                    <dt><p>CEO</p></dt>
                    <dd><p>榎本二郎</p></dd>
                </dl>
                <dl>
                    <dt><p>SEVICE</p></dt>
                    <dd>
                        <ul class="zt-list-mn">
                            <li>シェアオフィス開発、企画、運営</li>
                            <li>ビジネスマッチングサービス運営</li>
                            <li>WEBサイト制作、WEBシステム開発</li>
                            <li>WEBマーケティング</li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</section>
<!-- end of about page -->

<?php
get_footer();