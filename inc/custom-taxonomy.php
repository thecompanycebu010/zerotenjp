<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package TEMPLATE NAME
 */



//works-taxonomy
add_action( 'init', 'works_taxonomy', 0 );
 
function works_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Works Categories' ),
  ); 	
 
  register_taxonomy('works_taxonomies',array('works'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'works', 'with_front' => false ),
  ));
}
 

add_action( 'init', 'works_tags', 0 );
 
function works_tags() {
 
  $labels = array(
    'name' => _x( 'Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tags', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tags' ),
    'all_items' => __( 'All Tags' ),
    'parent_item' => __( 'Parent Tags' ),
    'parent_item_colon' => __( 'Parent Tags:' ),
    'edit_item' => __( 'Edit Tags' ), 
    'update_item' => __( 'Update Tags' ),
    'add_new_item' => __( 'Add New Tags' ),
    'new_item_name' => __( 'New Tags Name' ),
    'menu_name' => __( 'Works Tags' ),
  ); 	
 
  register_taxonomy('works_tags',array('works'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'works/tag', 'with_front' => false ),
  ));
}
 
