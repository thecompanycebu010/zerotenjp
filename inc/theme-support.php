<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package TEMPLATE NAME
 * 
 */


/*
 * ----------------------------------------------------------------------------------------
 *  THEME SUPPORT
 * ----------------------------------------------------------------------------------------
 */
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'qoute', 'status', 'video', 'audio', 'chat' ) );


/*
 * ----------------------------------------------------------------------------------------
 *  REGISTER THE CUSTOM NAV MENU
 * ----------------------------------------------------------------------------------------
 */
function register_navwalker(){
	require_once get_template_directory() . '/walker/class-wp-custom-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );

/*
 * ----------------------------------------------------------------------------------------
 *  REGISTER THE ACF PAGE OPTIONS
 * ----------------------------------------------------------------------------------------
 */
if(function_exists("register_options_page"))
{
 //Activate General ACF OPTIONS
 register_options_page('Theme Option Settings');
}


//Numeric Pagination
function wp_pagination() {
    global $wp_query;
    $big = 12345678;
    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'prev_text' => __('<img src="'. get_bloginfo('template_url') .'/assets/img/arrow-icon.png" />'),
        'next_text' => __('<img src="'. get_bloginfo('template_url') .'/assets/img/arrow-icon.png" />'),
    ) );
    if( is_array($page_format) ) {
                $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
                echo '<div class="zt-pagination"><ul>';
                foreach ( $page_format as $page ) {
                        echo "<li>$page</li>";
                }
                echo '</ul></div>';
    }
}


//Custom Taxonomy Category
function taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');
    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {
            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {
                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {
                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));
                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'taxonomy_slug_rewrite');



// Archive to Date
function wpa_date_posts_per_page( $query ) {
    if ( !is_admin()
        && $query->is_date()
        && $query->is_main_query() ) {
            $query->set( 'post_type', array( 'works' ));
    }
}
add_action( 'pre_get_posts', 'wpa_date_posts_per_page' );

// Change data structure
function wpd_change_date_structure(){
    global $wp_rewrite;
    $wp_rewrite->date_structure = 'archive/%year%';
}
add_action( 'init', 'wpd_change_date_structure' );