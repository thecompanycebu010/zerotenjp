<?php
/**
 * The template for displaying news detail page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<section class="zt-news-detail">
    <div class="cntr-1000">
        <div class="zt-news-date-cat">
            <span class="zt-det-date">2020.06.01</span>
            <span class="zt-det-cat">Media</span>
        </div>
        <h1 class="news-det-title">【メディア掲載】cross kumamoto（略称・クロくま）にThe Company熊本をご紹介いただきました</h1>
        <div class="news-det-img">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img02.jpg" alt="">
        </div>
        <div class="news-det-cont">
            <p>
            この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。<br><br>

            この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。
            </p>
        </div>
    </div>
</section>

<section class="zt-news-related-post">
    <div class="cntr-1000">
        <div class="zt-home-title">
            <h2>Related Post</h2>
        </div>
        <div class="gap gap-10 gap-0-xs">
            <div class="md-4 xs-12">
                <a href="#" class="zt-news-card">
                    <div class="zt-news-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img.jpg" alt="">
                    </div>
                    <div class="zt-news-cont">
                        <div class="zt-news-date-cat">
                            <span class="zt-news-date">2020.06.01</span><span class="zt-news-cat">Media</span>
                        </div>
                        <h4>【メディア掲載】cross kumamoto（略称・クロくま）にThe Company熊本をご紹介いただきました</h4>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="#" class="zt-news-card">
                    <div class="zt-news-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img.jpg" alt="">
                    </div>
                    <div class="zt-news-cont">
                        <div class="zt-news-date-cat">
                            <span class="zt-news-date">2020.06.01</span><span class="zt-news-cat">Media</span>
                        </div>
                        <h4>【メディア掲載】cross kumamoto（略称・クロくま）にThe Company熊本をご紹介いただきました</h4>
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="#" class="zt-news-card">
                    <div class="zt-news-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img.jpg" alt="">
                    </div>
                    <div class="zt-news-cont">
                        <div class="zt-news-date-cat">
                            <span class="zt-news-date">2020.06.01</span><span class="zt-news-cat">Media</span>
                        </div>
                        <h4>【メディア掲載】cross kumamoto（略称・クロくま）にThe Company熊本をご紹介いただきました</h4>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();