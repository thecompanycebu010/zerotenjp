<?php
/**
 * The template for displaying services page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<section class="zt-service-page">
    <div class="zt-home-title">
        <h2>Services</h2>
    </div>
    <div class="zt-service-wrp zt-round">
        <div class="cntr">
            <div class="gap">
                <div class="md-6 xs-12">
                    <div class="zt-service-page-img">
                        <h3 class="zt-service-tit">Creative</h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/services/img01-1.jpg" alt="">
                    </div>
                </div>
                <div class="md-6 xs-12">
                    <div class="zt-service-page-cont">
                        <h3 class="zt-service-tit">Creative</h3>
                        <p>アートとエンタテイメントの総合プロデュースファクトリー。商業施設や公共施設のプロジェクションマッピング、舞台や美術館のデジタル映像演出、イベント企画・制作、アートインスタレーション、デザインなど特定のカテゴリーに留まらない、広範な制作・表現活動を行っています。</p>
                        <div class="zt-read-more">
                            <a href="#" class="checkBox">
                                <span>Portfolio</span>
                                <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0" class="button" width="" height="65"/>
                                <rect x="0" y="22.5" class="box" width="20" height="20"/>
                                <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="zt-service-wrp zt-service-wrp-right zt-round2">
        <div class="cntr">
            <div class="gap">
                <div class="md-6 xs-12">
                    <div class="zt-service-page-cont">
                        <h3 class="zt-service-tit">CO-WORKING SPACE</h3>
                        <p>独自SNSで仕事が得られる。仕事仲間が増える。西日本最大級のコワーキング＆ワークスペース 「The Company」。ワークリンクとマルチロケーションをキーワードに、新しい働き方の提案と入居メンバーの事業成長のサポートを行っています。日本以外にも、アメリカ、シンガポール、タイ、ベトナム、フィリピンに拠点があり、グローバルな展開も加速中です。</p>
                        <div class="zt-read-more">
                            <a href="https://thecompany.jp/" class="checkBox" target="__blank">
                                <span>Official Site</span>
                                <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0" class="button" width="" height="65"/>
                                <rect x="0" y="22.5" class="box" width="20" height="20"/>
                                <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="md-6 xs-12">
                    <div class="zt-service-page-img">
                        <h3 class="zt-service-tit">CO-WORKING SPACE</h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/services/img01-2.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="zt-service-wrp zt-round3">
        <div class="cntr">
            <div class="gap">
                <div class="md-6 xs-12">
                    <div class="zt-service-page-img">
                        <h3 class="zt-service-tit">BUSINESS MATCHING</h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/services/img01-3.jpg" alt="">
                    </div>
                </div>
                <div class="md-6 xs-12">
                    <div class="zt-service-page-cont">
                        <h3 class="zt-service-tit">BUSINESS MATCHING</h3>
                        <p>新たなビジネスパートナーとの出会いをサポートする新サービス「Conected」。新しいプロジェクトを始める時、新たなサービスを創造する時、既存事業の変革を行う時、最も重要なのはチーム作り。福岡を中心に世界11拠点に展開するThe Companyのネットワークを活かし、無料でビジネスマッチングを行っています。</p>
                        <div class="zt-read-more">
                            <a href="https://connected.thecompany.jp/" class="checkBox" target="__blank">
                                <span>Official Site</span>
                                <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0" class="button" width="" height="65"/>
                                <rect x="0" y="22.5" class="box" width="20" height="20"/>
                                <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="zt-service-wrp zt-service-wrp-right zt-round4">
        <div class="cntr">
            <div class="gap">
                <div class="md-6 xs-12">
                    <div class="zt-service-page-cont">
                        <h3 class="zt-service-tit">WEB SOLUTION</h3>
                        <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</p>
                        <div class="zt-read-more">
                            <a href="#" class="checkBox" target="__blank">
                                <span>Official Site</span>
                                <svg width="" height="65" viewBox="0 0 240 65" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0" class="button" width="" height="65"/>
                                <rect x="0" y="22.5" class="box" width="20" height="20"/>
                                <polyline class="checkMark" points="4.5,32.6 8.7,36.8 16.5,29.1"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="md-6 xs-12">
                    <div class="zt-service-page-img">
                        <h3 class="zt-service-tit">WEB SOLUTION</h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/services/img01-4.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();