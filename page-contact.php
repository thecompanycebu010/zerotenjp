<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- contact -->
<section class="zt-contact">
    <div class="cntr-750">
        <div class="zt-home-title">
            <h2>Contact</h2>
        </div>
        <p class="zt-tag tc">
            フォームに必要事項を記入し、送信内容の確認に進んでください。
        </p>
        <?php echo do_shortcode('[mwform_formkey key="2694"]'); ?>
    </div>
</section>
<!-- end of contact -->

<?php
get_footer();