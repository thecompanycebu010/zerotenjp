<?php
/**
 * The template for displaying category pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<?php
    $news_cat_slug = get_queried_object()->slug;
    $news_cat_name = get_queried_object()->name;
?>

<!-- news page -->
<section class="zt-news-page">
    <div class="cntr-1000">
        <div class="zt-home-title">
            <h2>News</h2>
        </div>
        <div class="zt-categories">
            <h4>Category</h4>
            <div class="zt-news-select-cat">
                <div id="zt_selected_news">
                    <span>
                        
                        <?php echo $news_cat_name; ?>
                    </span>
                </div>
                <ul id="zt_news_cat_sel">
                    <li><a href="#" data-val="<?php bloginfo('url'); ?>/news/" class="zt_news_drop_item">All</a></li>
                    <?php
                        $terms = get_terms( 'category', array(
                            'orderby'    => 'id',
                            'order' => 'ASC',
                            'hide_empty' => 0,
                            'taxonomy' => 'category'
                        ) );
                        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                            foreach ( $terms as $term ) { ?>
                            
                            <li>
                                <a href="#" data-val="<?php echo get_category_link( $term->term_id ); ?>" class="zt_news_drop_item">
                                    <?php echo $term->name; ?>
                                </a>
                            </li>
                            <?php }
                        }
                    
                    ?>
                </ul>
            </div>
        </div>
        <div class="gap gap-10 gap-0-xs">
            
            <?php
                    global $post;
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    $args = array(
                        'paged' => $paged,
                        'posts_per_page' => 9, 
                        'orderby' => 'date', 
                        'order' => 'DESC', 
                        'post_type' => 'post',
                        'tax_query' => array(
                            array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => $news_cat_slug
                            ),
                        ),
                    );
                    $my_query = new WP_Query($args);
                    $max_num_pages = $my_query->max_num_pages; 
                ?>

                <?php if( $my_query -> have_posts() ) : while($my_query -> have_posts()) : $my_query -> the_post(); ?>
                    <div class="md-12 xs-12">
                        <a href="<?php the_permalink(); ?>" class="zt-news-card">
                            <div class="zt-news-cont">
                                <div class="zt-news-date-cat">
                                    <span class="zt-news-date"><?php the_date('Y.m.d'); ?></span>
                                    <?php
                                        $terms = get_the_terms( $post->ID , 'category' );
                                        if(is_array($terms) || is_object($terms)){
                                            foreach ( $terms as $term ) {
                                                ?>
                                                <span class="zt-news-cat <?php echo $term->slug; ?>">
                                                    <?php echo $term->name; ?>
                                                </span>
                                            <?php
                                            }
                                        }
                                    ?>
                                </div>
                                <h4><?php the_title(); ?></h4>
                            </div>
                        </a>
                    </div>
                <?php endwhile; endif; ?>


            <?php wp_pagination(); ?>
        </div>
    </div>
</section>
<!-- end of news page -->

    

<?php
get_footer();
?>