<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zeroten
 */

get_header();
?>

<?php
    $query_cs = new WP_Query(
    array(
        'taxonomy' => 'works_taxonomies',
        'post_type'     =>'works',
        'post_status'   =>'publish',
        'posts_per_page' => -1,
        'orderby'        => 'publish_date',
        'order'         => 'DESC'
    ));
?>
<!-- works page -->
<!-- sp filter -->
<div class="cat-opn">
    <ul class="zt-works-cntr ct-lst sp">
        <li class="mb-0 mb-40-xs">
            <div class="cat-close">
                <div class="list-cat">
                    <div class="zt-filter">
                        <h3>FILTER</h3>
                    </div>
                </div>
                <a href="javascript:;" class="sp-toggle-cat"></a>
            </div>
            <div class="cat-open">
                <div class="c-list">
                    <div class="gap gap-80 gap-0-xs">
                        <div class="xs-6">
                            <div class="zt-year-sp">
                                <h4><span>year</span></h4>
                            </div>
                            <ul class="zt-year">
                                <li>2019</li>
                            </ul>
                        </div>
                        <div class="xs-6">
                            <div class="zt-year-sp">
                                <h4><span>category</span></h4>
                            </div>
                            <ul class="zt-category">
                                <?php
                                    $terms = get_terms( 'works_taxonomies', array(
                                        'orderby'    => 'count',
                                        'hide_empty' => 0
                                    ) );
                                    foreach($terms as $term)
                                    {
                                        echo '<li>';
                                        $term_link = get_term_link( $term );
                                        echo '<a href="' . $term_link . '">' . $term->name . '</a>' . ' ';
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <div class="xs-12">
                            <div class="zt-year-sp">
                                <h4 class="is-a"><span>tag</span></h4>
                            </div>
                            <div class="gap gap-20 gap-0-xs">
                                <ul class="zt-tag">
                                    <?php
                                        $terms = get_terms( 'works_tags', array(
                                            'orderby'    => 'count',
                                            'hide_empty' => 0
                                        ) );
                                        foreach($terms as $term)
                                        {
                                            echo '<li>';
                                            $term_link = get_term_link( $term );
                                            echo '' . $term->name . ' ';
                                            echo '</li>';
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </li>       
    </ul>  
</div>
<!-- end of dp filter-->

<section class="zt-works-page" onclick="myFunction()">
   <div class="zt-wrks-bg">
        <div class="cntr">
            <div class="gap gap-80 gap-0-xs">
                <div class="md-3 xs-12">
                    <h3><span>year</span></h3>
                </div>
                <div class="md-3 xs-12">
                    <h3><span>category</span></h3>
                </div>
                <div class="md-6 xs-12">
                    <h3 class="is-a"><span>tag</span></h3>
                </div>
            </div>
        </div>
   </diV>
   <div id="zt-shw">
        <div class="cntr">
            <div class="gap gap-80 gap-0-xs">
                <div class="md-3 xs-12">
                    <ul class="zt-year">
                        <?php 
                                $query = $wpdb->prepare('
                                SELECT YEAR(%1$s.post_date) AS `year`, count(%1$s.ID) as `posts`
                                FROM %1$s
                                WHERE %1$s.post_type IN ("works")
                                AND %1$s.post_status IN ("publish")
                                GROUP BY YEAR(%1$s.post_date)
                                ORDER BY %1$s.post_date DESC',
                                $wpdb->posts
                            );
                            $results = $wpdb->get_results($query);
                            $years = array();
                            if(!empty($results)) : foreach($results as $result) :
                                $url = get_year_link($result->year); 
                                $text = '<li>' . $result->year . '</li>';   
                                $years[] = get_archives_link($url, $text, 'html');                    
                            endforeach;
                            endif;
                            echo join("\n", $years);
                        ?>
                    </ul>
                </div>
                <div class="md-3 xs-12">  
                    <ul class="zt-category">
                        <?php
                            $terms = get_terms( 'works_taxonomies', array(
                                'orderby'    => 'count',
                                'hide_empty' => 0
                            ) );
                            foreach($terms as $term)
                            {
                                echo '<li>';
                                $term_link = get_term_link( $term );
                                echo '<a href="' . $term_link . '">' . $term->name . '</a>' . ' ';
                                echo '</li>';
                            }
                        ?>
                    </ul>
                </div>
                <div class="md-6 xs-12">
                   
                    <ul class="zt-tag">
                        <?php
                            $terms = get_terms( 'works_tags', array(
                                'orderby'    => 'count',
                                'hide_empty' => 0
                            ) );
                            foreach($terms as $term)
                            {
                                echo '<li>';
                                echo '' . $term->name . ' ';
                                echo '</li>';
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
   </div>

</section>
<!-- end of works page -->

<div class="zt-works-cntr">
    <ul class="grid">
        <?php
                $count = 1;
                if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                $temp = $wp_query;
                $wp_query = null;
                $args = array( 'post_type' => 'works', 'order'=>'DESC', 'posts_per_page' => -1, 'paged' => $paged);
                $wp_query = new WP_Query();
                $wp_query->query( $args );
                while ($wp_query->have_posts()) : $wp_query->the_post();
            ?>

                <div class="grid-item fadeInUp">
                    <a href="<?=the_permalink();?>" class="m-cs_card">
                            <div class="zt-ovrly-cntr mb-20 mb-20-xs">
                            <?php if(has_post_thumbnail()) : ?>
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                            <?php else: ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img.jpg" alt="" class="is-wide">
                            <?php endif; ?>
                            <div class="zt-overlay">
                                <div class="zt-txt">
                                    <span class="zt-dt-tm"><?php the_time('Y m.d'); ?> - <?php the_time('H.i'); ?></span>
                                    <h3 class="is-a"><?=the_title();?></h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
        <?php endwhile; ?>
    </ul>
</div>





<script>
    function myFunction() {
    var x = document.getElementById("zt-shw");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
    }
</script>

<script>
   $(function(){
        function animation(){
            $('.fadeInUp').each(function(){
            //ターゲットの位置を取得
            var target = $(this).offset().top;
            //スクロール量を取得
            var scroll = $(window).scrollTop();
            //ウィンドウの高さを取得
            var windowHeight = $(window).height();
            //ターゲットまでスクロールするとフェードインする
            if (scroll > target - windowHeight){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
            });
        }
        animation();
        $(window).scroll(function (){
            animation();
        });
    });
</script>


<?php
get_footer();