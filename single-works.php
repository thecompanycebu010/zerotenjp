<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zeroten
 */

get_header();
?>

<?php
    $query_cs = new WP_Query(
    array(
        'taxonomy' => 'works_taxonomies',
        'post_type'     =>'product-item',
        'post_status'   =>'publish',
        'posts_per_page' => -1,
        'orderby'        => 'publish_date',
        'order'         => 'DESC'
    ));
?>

<!-- works-detail page -->
<section class="zt-works-dtl-page">
       <div class="zt-sec1">
            <div class="cntr">
                <div class="zt-img">
                    <?php if(has_post_thumbnail()) : ?>
                         <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                    <?php else: ?>
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news-img.jpg" alt="" class="is-wide">
                    <?php endif; ?>
                </div>
            </div>
            <div class="zt-grd cntr">
                <div class="zt-grd-4 zt-dtl-wrkdtl">
                    <div class="zt-dt-wrkdtl">
                        <p><?php $post_date = get_the_date( 'M, Y' ); echo $post_date; ?></p>
                    </div>
                    <div class="zt-cat-wrkdtl">
                        <?php
                            $categories = get_the_terms( $post->ID, 'works_taxonomies' );
                            $cat_name = $categories[0]->name;
                        ?>
                        <p>category<span><?php echo $cat_name; ?></span></p>
                    </div>
                    <div class="zt-tag-wrkdtl">
                        <dl class="zt-dl-tag">
                            <dt>tag</dt>
                            <dd>
                                <ul class="zt-wrkdtl-tags">
                                    <?php
                                    $tags = get_the_terms( $post->ID, 'works_tags' );
                                    if ( $tags ) :
                                        foreach ( $tags as $tag ) : ?>
                                            <li><span><?php echo esc_html( $tag->name ); ?></span></li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="zt-grd-8">
                    <div class="zt-lft-bg">
                        <div class="zt-ttl">
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <div class="zt-mn-cntnts cntr">
                            <?php if( get_field('text') ): ?>
                                <p><?php the_field('text'); ?></p>
                            <?php endif; ?>
                            <?php if( get_field('hpurl') ): ?>
                                <p><a href="<?php the_field('hpurl'); ?>"><?php the_field('hpurl'); ?></a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
       </div>
       <?php if( get_field('image1') ): ?>
            <div class="cntr zt-plpr">   
                <div class="zt-sec2">
                    <div class="zt-m-zr"></div>
                        <div class="zt-grd">
                            <div class="zt-grd-6">
                                <div class="zt-wrdtl-bg">
                                    <img src="<?php the_field('image1'); ?>" class="is-wide" />
                                </div>
                            </div>
                            <div class="zt-grd-4">
                                <?php if( get_field('image2') ): ?>
                                    <img src="<?php the_field('image2'); ?>" class="is-wide" />
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
            </div>
            <?php endif; ?>    
    </div>   
    <!-- <div class="zt-wrkdtl-z-2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/icons/ico_02.png" alt="">
    </div> -->
    <?php if( get_field('image3') ): ?>
        <div class="zt-sec3">
        <div class="zt-m-zr2"></div>
            <div class="cntr">
                <div class="zt-img1">
                    <img src="<?php the_field('image3'); ?>" class="is-wide" />
                </div>
                <!-- <div class="zt-wrkdtl-z-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/zero.png" alt="">
                </div> -->
                <div class="zt-img2">
                    <?php if( get_field('image4') ): ?>
                        <img src="<?php the_field('image4'); ?>" class="is-wide" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>    
    <?php if( get_field('image5') ): ?>
        <div class="zt-sec4 cntr">
            <div class="zt-img1">
                <img src="<?php the_field('image5'); ?>">
            </div>
            <div class="zt-grd zt-img2">
                <div class="zt-grd-6">
                    <?php if( get_field('image6') ): ?>
                        <img src="<?php the_field('image6'); ?>">
                    <?php endif; ?>
                    <div class="zt-bg"></div>
                </div>
                <?php if( get_field('image7') ): ?>
                    <div class="zt-grd-4">
                        <img src="<?php the_field('image7'); ?>">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if( get_field('image8') ): ?>
        <div class="zt-sec5 cntr">
            <div class="zt-img1">
                <img src="<?php the_field('image8'); ?>">
            </div>
            <div class="zt-wrkdtl-z-5">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/works/icons/ico_01.png" alt="">
            </div>
            <?php if( get_field('image9') ): ?>
                <div class="zt-grd zt-img2">
                    <div class="zt-grd-6">
                        <img src="<?php the_field('image9'); ?>">
                    </div>
                <?php endif; ?>
                <?php if( get_field('image10') ): ?>
                    <div class="zt-grd-4">
                        <img src="<?php the_field('image10'); ?>">
                    </div>
                <?php endif; ?>
            </div>
        </div> 
    <?php endif; ?>        
   
   
</section>
<!-- end of works-detail page -->
<div class="cntr">
    <button class="zt-wrk-dtl-btn">back</button>
</diV>
<?php
get_footer();