<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TEMPLATENAME
 */

?>

	</main>

	<footer class="zt-footer">
		<ul class="zt-footer-social">
			<li>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/fb.png" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/insta.png" alt="">
				</a>
			</li>
			<li>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon/yt.png" alt="">
				</a>
			</li>
		</ul>
		<ul class="zt-copyright">
			<li>(C) 2020 Zero-Ten inc. / Zero-Ten Park inc.</li>
			<li>
				<a href="#">Privacy Policy</a>
			</li>
		</ul>
	</footer>

	<?php wp_footer(); ?>
	<?php 
		$custom_js = get_option( 'theme_js' );
		if(!empty($custom_js)) {
			?>
				<?php echo '<script>'. $custom_js. '</script> '; ?>
			<?php
		}
	?>
	</body>
</html>