<?php
/**
 * The template for displaying news pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TEMPLATENAME
 */

get_header();
?>

<!-- news page -->
<section class="zt-news-page">
    <div class="zt-bg01">
        <span></span>
    </div>
    <div class="zt-bg02">
        <span></span>
    </div>
    <div class="zt-bg03">
        <span></span>
    </div>
    <div class="cntr-1000">
        <div class="zt-home-title">
            <h2>News</h2>
        </div>
        <div class="zt-categories">
            <h4>Category</h4>
            <div class="zt-news-select-cat">
                <div id="zt_selected_news">
                    <span>
                        All
                    </span>
                </div>
                <ul id="zt_news_cat_sel">
                    <li><a href="#" data-val="<?php bloginfo('url'); ?>/news/" class="zt_news_drop_item">All</a></li>
                    <?php
                        $terms = get_terms( 'category', array(
                            'orderby'    => 'id',
                            'order' => 'ASC',
                            'hide_empty' => 0,
                            'taxonomy' => 'category'
                        ) );
                        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                            foreach ( $terms as $term ) { ?>
                            
                            <li>
                                <a href="#" data-val="<?php echo get_category_link( $term->term_id ); ?>" class="zt_news_drop_item">
                                    <?php echo $term->name; ?>
                                </a>
                            </li>
                            <?php }
                        }
                    
                    ?>
                </ul>
            </div>
        </div>
        <div class="gap gap-10 gap-0-xs">
            <?php
                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } else if ( get_query_var('page') ) {$paged = get_query_var('page'); } else {$paged = 1; }
                    $temp = $wp_query;
                    $wp_query = null;
                    $args = array( 'post_type' => 'post', 'order'=>'DESC', 'posts_per_page' => 11, 'paged' => $paged);
                    $wp_query = new WP_Query();
                    $wp_query->query( $args );
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                ?>

                <div class="md-12 xs-12">
                    <a href="<?php the_permalink(); ?>" class="zt-news-card">
                        <div class="zt-news-cont">
                            <div class="zt-news-date-cat">
                                <span class="zt-news-date"><?php the_date('Y.m.d'); ?></span>
                                <?php
                                    $terms = get_the_terms( $post->ID , 'category' );
                                    if(is_array($terms) || is_object($terms)){
                                        foreach ( $terms as $term ) {
                                            ?>
                                            <span class="zt-news-cat <?php echo $term->slug; ?>">
                                                <?php echo $term->name; ?>
                                            </span>
                                        <?php
                                        }
                                    }
                                ?>
                            </div>
                            <h4><?php the_title(); ?></h4>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php wp_pagination(); ?>
        </div>
    </div>
</section>
<!-- end of news page -->

<?php
get_footer();